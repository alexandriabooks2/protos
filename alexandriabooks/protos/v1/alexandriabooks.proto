syntax = "proto3";

package alexandriabooks.protos.v1;

message PluginInstance {
  string plugin_instance_id = 1;
  bool running = 2;
  Status status = 3;
  string details = 4;
}

message PluginDefinition {
  string plugin_id = 1;
  string name = 2;
  string description = 3;
  repeated PluginInstance instances = 4;
}

enum LogLevel {
  LOG_LEVEL_UNSPECIFIED = 0;
  LOG_LEVEL_DEBUG = 1;
  LOG_LEVEL_INFO = 2;
  LOG_LEVEL_WARNING = 3;
  LOG_LEVEL_ERROR = 4;
}

message ConfigValue {
  string key = 1;
  string name = 2;
  string description = 3;
  oneof value {
    string string_value = 4;
    int32 int32_value = 5;
    int64 int64_value = 6;
    float float_value = 7;
    bool bool_value = 8;
    NestedConfigValue nested_value = 20;
  }
}

message NestedConfigValue {
  repeated ConfigValue values = 1;
}

enum PersonRole {
  PERSON_ROLE_UNSPECIFIED = 0;
  PERSON_ROLE_WRITER = 1;
  PERSON_ROLE_ARTIST = 2;
  PERSON_ROLE_COVER_ARTIST = 3;
  PERSON_ROLE_LETTERER = 4;
  PERSON_ROLE_COLORIST = 5;
  PERSON_ROLE_PENCILLER = 6;
  PERSON_ROLE_EDITOR = 7;
  PERSON_ROLE_TRANSLATOR = 8;
}

message Date {
  uint32 year = 1;
  uint32 month = 2;
  uint32 day = 3;
}

message UniqueIdentifier {
  string namespace = 1;
  string identifier = 2;
}

message Person {
  string source_id = 1;
  string person_id = 2;

  string full_name = 3;
  string given_names = 4;
  string last_name = 5;
  repeated PersonRole roles = 6;
  Date birth = 7;
  Date death = 8;
  Image poster = 9;

  repeated UniqueIdentifier external_identifiers = 10;
}

message Tag {
  string key = 1;
  string value = 2;
}

message Image {
  string mimetype = 1;
  bytes image_data = 2;
}

message FileType {
  string name = 1;
  repeated string extensions = 2;
  repeated string mimetypes = 3;
}

enum SourceValidity {
  SOURCE_VALIDITY_UNSPECIFIED = 0;
  SOURCE_VALIDITY_VALID = 1;
  SOURCE_VALIDITY_INVALID = 2;
}

enum CollectionType {
  COLLECTION_TYPE_UNSPECIFIED = 0;
  COLLECTION_TYPE_SERIES = 1;
}

message Collection {
  string source_id = 1;
  string collection_id = 2;
  string title = 3;
  string description = 4;
  repeated Tag tags = 5;
  Image poster = 6;
  Image banner = 7;

  CollectionType type = 8;

  repeated UniqueIdentifier external_identifiers = 9;
}

message CollectionParticipant {
  Collection collection = 1;

  repeated string volumes = 2;
  repeated string issues = 3;
  uint32 position = 4;
}

message VfsPath {
  repeated string name_parts = 1;
}

message SourceMetadata {
  string source_id = 1;
  string book_id = 2;
  string source_plugin_instance_id = 3;

  string title = 4;
  string subtitle = 5;
  repeated Person people = 6;
  string publisher = 7;
  string imprint = 8;
  string rights = 9;
  Date publish_date = 10;
  repeated string languages = 11;
  repeated string countries = 12;
  uint32 minimum_age = 13;
  string description = 14;
  repeated string genres = 15;
  repeated Tag tags = 16;
  Date acquisition_date = 17;
  Image front_cover = 18;
  Image back_cover = 19;
  repeated CollectionParticipant in_collections = 28;

  string local_file_path = 20;
  string temporary_local_file_path = 25;
  FileType file_type = 21;
  uint64 file_size = 29;

  uint32 page_count = 22;
  uint32 chapter_count = 23;
  uint64 word_count = 24;
  uint32 audio_length_seconds = 26;

  repeated UniqueIdentifier external_identifiers = 27;
}

message TranscodeResult {
  string transcode_url = 1;
  string source_id = 2;
  repeated ConfigValue config = 3;
}

enum PluginInstallType {
  PLUGIN_INSTALL_TYPE_UNSPECIFIED = 0;
  PLUGIN_INSTALL_TYPE_PYTHON = 1;
}

message InstallPluginTypeRequest {
  PluginInstallType install_type = 1;
  string source = 2;
}

message InstallPluginTypeResponse {
  string install_id = 1;
}

message GetPluginInstallStatusRequest {
  string install_id = 1;
}

message GetPluginInstallStatusResponse {
  string details = 1;
  bool done = 2;
  bool successful = 3;
  float progress = 4;
}

message CancelPluginInstallRequest {
  string install_id = 1;
}

message CancelPluginInstallResponse {
}

message GetPluginsRequest {
  repeated string plugin_ids = 1;
}

message GetPluginsResponse {
  repeated PluginDefinition definitions = 1;
}

message NewPluginRequest {
  string plugin_id = 1;
  string plugin_instance_id = 2;
  repeated ConfigValue config = 3;
  LogLevel log_level = 4;
}

message NewPluginResponse {

}

message DeletePluginRequest {
  string plugin_instance_id = 1;
}

message DeletePluginResponse {
  
}

message StartPluginRequest {
  string plugin_instance_id = 1;
}

message StartPluginResponse {}

message StopPluginRequest {
  string plugin_instance_id = 1;
}

message StopPluginResponse {
}

message RestartPluginRequest {
  string plugin_instance_id = 1;
}

message RestartPluginResponse {}

enum PluginServiceType {
  PLUGIN_SERVICE_TYPE_UNSPECIFIED = 0;
  PLUGIN_SERVICE_TYPE_LOCAL_STORE = 1;
  PLUGIN_SERVICE_TYPE_REMOTE_STORE = 2;
  PLUGIN_SERVICE_TYPE_METADATA = 3;
  PLUGIN_SERVICE_TYPE_TRANSCODER = 4;
  PLUGIN_SERVICE_TYPE_TRANSFER = 5;
  PLUGIN_SERVICE_TYPE_VFS = 6;
}

message RegisterPluginServiceRequest {
  string plugin_instance_id = 1;
  PluginServiceType service_type = 2;
  uint32 port = 3;
  string name = 4;
}

message RegisterPluginServiceResponse {

}

enum Status {
  STATUS_UNSPECIFIED = 0;
  STATUS_INITIALIZING = 1;
  STATUS_READY = 2;
  STATUS_STOPPING = 3;
  STATUS_STOPPED = 4;
  STATUS_ERROR = 5;
}

message StatusUpdatesRequest {
  string plugin_instance_id = 1;
  Status status = 2;
  string details = 3;
}

message StatusUpdatesResponse {
  Status status = 1;
  string details = 2;
}

message GetConfigurationRequest {
  string plugin_instance_id = 1;
}

message GetConfigurationResponse {
  repeated ConfigValue config = 1;
  LogLevel log_level = 2;
}

message SetConfigurationRequest {
  string plugin_instance_id = 1;
  repeated ConfigValue config = 2;
  LogLevel log_level = 3;
}

message SetConfigurationResponse {
}

message RegisterSupportedFileTypesRequest {
  string plugin_instance_id = 1;
  repeated FileType file_types = 2;
}

message RegisterSupportedFileTypesResponse {
}

message GetSupportedFileTypesRequest {
}

message GetSupportedFileTypesResponse {
  repeated FileType file_types = 1;
}

message NewSourceMetadataRequest {
  SourceMetadata metadata = 1;
  SourceValidity validity = 2;
}

message NewSourceMetadataResponse {

}

message RemoveSourceMetadataRequest {
  string source_id = 1;
}

message RemoveSourceMetadataResponse {

}

message SearchRequest {
  oneof search_reference {
    string terms = 1;
    string cursor = 2;
  }
  int32 offset = 3;
  int32 count = 4;

  // Include only these plugins in the search
  repeated string plugin_instance_id = 5;
}

message SearchResponse {
  SourceMetadata effective_metadata = 1;
  repeated string child_source_ids = 2;
  string plugin_instance_id = 3;
  string cursor = 4;
  uint64 position = 5;
  uint64 estimated_total = 6;
}

message SearchSource {
  string plugin_instance_id = 1;
  string name = 2;
}

message GetSearchSourcesRequest {
}

message GetSearchSourcesResponse {
  repeated SearchSource sources = 1;
}

message BrowseVfsRequest {
  VfsPath path = 1;
  string cursor = 2;

  int32 offset = 3;
  int32 count = 4;
}

message BrowseVfsResponse {
  repeated string subdirs = 1;
  repeated string books = 2;
  string plugin_instance_id = 3;
  string cursor = 4;
  uint64 position = 5;
  uint64 estimated_total = 6;
}

message VfsMetadataLookupRequest {
  SourceMetadata effective_metadata = 1;
}

message VfsMetadataLookupResponse {
  repeated VfsPath path = 1;
}

message StartBookDownloadRequest {
  string plugin_instance_id = 1;
  string source_id = 2;
}

message StartBookDownloadResponse {
  DownloadRecord record = 1;
}

message CancelBookDownloadRequest {
  string plugin_instance_id = 1;
  string download_id = 2;
}

message CancelBookDownloadResponse {

}

message DownloadRecord {
  string plugin_instance_id = 1;
  string book_id = 2;
  string source_id = 3;
  string download_id = 4;
  bool complete = 5;
  string local_path = 6;
  float progress = 7;
  string details = 8;
  string error = 9;
}

message GetBookDownloadsRequest {
  string plugin_instance_id = 1;
  string book_id = 2;
  string source_id = 3;
  string download_id = 4;
}

message GetBookDownloadsResponse {
  repeated DownloadRecord records = 1;
}

message GetBookMetadataRequest {
 string book_id = 1;
 string plugin_instance_id = 2;
}

message GetBookMetadataResponse {
  SourceMetadata effective_metadata = 1;
  repeated string child_source_ids = 2;
}

message GetSourceMetadataRequest {
 repeated string source_ids = 1;
 string plugin_instance_id = 2;
}

message GetSourceMetadataResponse {
  repeated SourceMetadata metadata = 1;
}

message GetUserMetadataRequest {
  string book_id = 1;
}

message GetUserMetadataResponse {
  SourceMetadata metadata = 1;
}

message SetUserMetadataRequest {
  string book_id = 1;
  SourceMetadata metadata = 2;
}

message SetUserMetadataResponse {
}

message GetTransferTargetsRequest {

}

message TransferTarget {
  string plugin_instance_id = 1;
  string target_id = 2;
  string name = 3;
}

message GetTransferTargetsResponse {
  repeated TransferTarget targets = 1;
}

message TransferBookRequest {
  string plugin_instance_id = 1;
  SourceMetadata metadata = 2;
  string temporary_path = 3;
  string target_id = 4;
}

message TransferBookResponse {
  SourceMetadata metadata = 1;
}

message GetTranscodeTargetsRequest {
  SourceMetadata metadata = 1;
}

message TranscodeTarget {
  string plugin_instance_id = 1;
  FileType file_type = 2;
  repeated ConfigValue config = 3;
}

message GetTranscodeTargetsResponse {
  repeated TranscodeTarget targets = 1;
}

message StartBookTranscodeRequest {
  string src_path = 1;
  string dest_path = 2;
  FileType file_type = 3;
  repeated ConfigValue config = 4;
}

message StartBookTranscodeResponse {
  string transcode_id = 1;
}

message CancelBookTranscodeRequest {
  string transcode_id = 1;
}

message CancelBookTranscodeResponse {

}

message TranscodeRecord {
  string plugin_instance_id = 1;
  string transcode_id = 2;
  bool complete = 3;
  string local_path = 4;
  float progress = 5;
  string details = 6;
}

message GetBookTranscodesRequest {

}

message GetBookTranscodesResponse {
  repeated TranscodeRecord records = 1;
}

service PluginHostService {
  rpc InstallPluginType(InstallPluginTypeRequest) returns (InstallPluginTypeResponse);
  rpc GetPluginInstallStatus(GetPluginInstallStatusRequest) returns (GetPluginInstallStatusResponse);
  rpc CancelPluginInstall(CancelPluginInstallRequest) returns (CancelPluginInstallResponse);
  rpc GetPlugins(GetPluginsRequest) returns (GetPluginsResponse);
  rpc NewPlugin(NewPluginRequest) returns (NewPluginResponse);
  rpc DeletePlugin(DeletePluginRequest) returns (DeletePluginResponse);
  rpc StartPlugin(StartPluginRequest) returns (StartPluginResponse);
  rpc StopPlugin(StopPluginRequest) returns (StopPluginResponse);
  rpc RestartPlugin(RestartPluginRequest) returns (RestartPluginResponse);
  rpc RegisterPluginService(RegisterPluginServiceRequest) returns (RegisterPluginServiceResponse);

  rpc StatusUpdates(stream StatusUpdatesRequest) returns (stream StatusUpdatesResponse);

  rpc GetConfiguration(GetConfigurationRequest) returns (GetConfigurationResponse);
  rpc SetConfiguration(SetConfigurationRequest) returns (SetConfigurationResponse);

  rpc RegisterSupportedFileTypes(RegisterSupportedFileTypesRequest) returns (RegisterSupportedFileTypesResponse);
  rpc GetSupportedFileTypes(GetSupportedFileTypesRequest) returns (GetSupportedFileTypesResponse);

  rpc NewSourceMetadata(stream NewSourceMetadataRequest) returns (NewSourceMetadataResponse);
  rpc RemoveSourceMetadata(stream RemoveSourceMetadataRequest) returns (RemoveSourceMetadataResponse);

  rpc Search(SearchRequest) returns (stream SearchResponse);
  rpc GetSearchSources(GetSearchSourcesRequest) returns (GetSearchSourcesResponse);

  rpc BrowseVfs(BrowseVfsRequest) returns (BrowseVfsResponse);
  rpc VfsMetadataLookup(VfsMetadataLookupRequest) returns (VfsMetadataLookupResponse);

  rpc StartBookDownload(StartBookDownloadRequest) returns (StartBookDownloadResponse);
  rpc CancelBookDownload(CancelBookDownloadRequest) returns (CancelBookDownloadResponse);
  rpc GetBookDownloads(GetBookDownloadsRequest) returns (GetBookDownloadsResponse);

  rpc GetBookMetadata(GetBookMetadataRequest) returns (GetBookMetadataResponse);
  rpc GetSourceMetadata(GetSourceMetadataRequest) returns (GetSourceMetadataResponse);
  rpc GetUserMetadata(GetUserMetadataRequest) returns (GetUserMetadataResponse);
  rpc SetUserMetadata(SetUserMetadataRequest) returns (SetUserMetadataResponse);

  rpc GetTransferTargets(GetTransferTargetsRequest) returns (GetTransferTargetsResponse);
  rpc TransferBook(TransferBookRequest) returns (TransferBookResponse);

  rpc GetTranscodeTargets(GetTranscodeTargetsRequest) returns (GetTranscodeTargetsResponse);
  rpc StartBookTranscode(StartBookTranscodeRequest) returns (StartBookTranscodeResponse);
  rpc CancelBookTranscode(CancelBookTranscodeRequest) returns (CancelBookTranscodeResponse);
  rpc GetBookTranscodes(GetBookTranscodesRequest) returns (GetBookTranscodesResponse);
}

message LocalSearchRequest {
  oneof search_reference {
    string terms = 1;
    string cursor = 2;
  }
  int32 offset = 3;
  int32 count = 4;
}

message LocalSearchResponse {
  SourceMetadata effective_metadata = 1;
  repeated string child_source_ids = 2;
  string plugin_instance_id = 3;
  string cursor = 4;
  uint64 position = 5;
  uint64 estimated_total = 6;
}

message NewLocalSourceMetadataRequest {
  SourceMetadata metadata = 1;
}

message NewLocalSourceMetadataResponse {

}

message RemoveLocalSourceMetadataRequest {
  string source_id = 1;
}

message RemoveLocalSourceMetadataResponse {

}

message GetLocalBookMetadataRequest {
  string book_id = 1;
}
 
message GetLocalBookMetadataResponse {
  SourceMetadata effective_metadata = 1;
  repeated string child_source_ids = 2;
}

message GetLocalSourceMetadataRequest {
  repeated string source_ids = 1;
}
 
message GetLocalSourceMetadataResponse {
  repeated SourceMetadata metadata = 1;
}
 
message GetLocalUserMetadataRequest {
  string book_id = 1;
}
 
message GetLocalUserMetadataResponse {
  SourceMetadata metadata = 1;
}

message SetLocalUserMetadataRequest {
  string book_id = 1;
  SourceMetadata metadata = 2;
}
 
message SetLocalUserMetadataResponse {
}

service LocalStoreService {
  rpc LocalSearch(LocalSearchRequest) returns (stream LocalSearchResponse);

  rpc NewLocalSourceMetadata(stream NewLocalSourceMetadataRequest) returns (NewLocalSourceMetadataResponse);
  rpc RemoveLocalSourceMetadata(stream RemoveLocalSourceMetadataRequest) returns (RemoveLocalSourceMetadataResponse);

  rpc GetLocalBookMetadata(GetLocalBookMetadataRequest) returns (GetLocalBookMetadataResponse);
  rpc GetLocalSourceMetadata(GetLocalSourceMetadataRequest) returns (GetLocalSourceMetadataResponse);
  rpc GetLocalUserMetadata(GetLocalUserMetadataRequest) returns (GetLocalUserMetadataResponse);
  rpc SetLocalUserMetadata(SetLocalUserMetadataRequest) returns (SetLocalUserMetadataResponse);
}

message GetServiceTransferTargetsRequest {

}

message GetServiceTransferTargetsResponse {
  repeated TransferTarget targets = 1;
}

message TransferBookToServiceRequest {
  SourceMetadata metadata = 1;
  string temporary_path = 2;
  string target_id = 3;
}

message TransferBookToServiceResponse {
  SourceMetadata metadata = 1;
}

service BookTransferService {
  rpc GetServiceTransferTargets(GetServiceTransferTargetsRequest) returns (GetServiceTransferTargetsResponse);
  rpc TransferBookToService(TransferBookToServiceRequest) returns (TransferBookToServiceResponse);
}

message RemoteSearchRequest {
  oneof search_reference {
    string terms = 1;
    string cursor = 2;
  }
  int32 offset = 3;
  int32 count = 4;
}

message RemoteSearchResponse {
  SourceMetadata effective_metadata = 1;
  repeated string child_source_ids = 2;
  string plugin_instance_id = 3;
  string cursor = 4;
  uint64 position = 5;
  uint64 estimated_total = 6;
}

message GetRemoteBookMetadataRequest {
  string book_id = 1;
}

message GetRemoteBookMetadataResponse {
  SourceMetadata effective_metadata = 1;
  repeated string child_source_ids = 2;
}

message GetRemoteSourceMetadataRequest {
  repeated string source_ids = 1;
}
 
message GetRemoteSourceMetadataResponse {
  repeated SourceMetadata metadata = 1;
}

message StartRemoteBookDownloadRequest {
  string source_id = 1;
}

message StartRemoteBookDownloadResponse {
  DownloadRecord record = 1;
}

message CancelRemoteBookDownloadRequest {
  string download_id = 1;
}

message CancelRemoteBookDownloadResponse {

}

message GetRemoteBookDownloadsRequest {
  string plugin_instance_id = 1;
  string book_id = 2;
  string source_id = 3;
  string download_id = 4;
}

message GetRemoteBookDownloadsResponse {
  repeated DownloadRecord records = 1;
}

service RemoteStoreService {
  // Searches the remote store
  rpc RemoteSearch(RemoteSearchRequest) returns (stream RemoteSearchResponse);

  // Gets the metadata for a single book
  rpc GetRemoteBookMetadata(GetRemoteBookMetadataRequest) returns (GetRemoteBookMetadataResponse);

  // Gets the specific source metadata for a book source
  rpc GetRemoteSourceMetadata(GetRemoteSourceMetadataRequest) returns (GetRemoteSourceMetadataResponse);

  // Starts the download of the book
  rpc StartRemoteBookDownload(StartRemoteBookDownloadRequest) returns (StartRemoteBookDownloadResponse);

  // Cancels an in-progress download
  rpc CancelRemoteBookDownload(CancelRemoteBookDownloadRequest) returns (CancelRemoteBookDownloadResponse);

  // Gets the status of current downloads
  rpc GetRemoteBookDownloads(GetRemoteBookDownloadsRequest) returns (GetRemoteBookDownloadsResponse);
}

message GatherMetadataRequest {
  SourceMetadata metadata = 1;
  SourceValidity validity = 2;
}

message GatherMetadataResponse {
  SourceMetadata metadata = 1;
  SourceValidity validity = 2;
}

service MetadataService {
  rpc GatherMetadata(stream GatherMetadataRequest) returns (stream GatherMetadataResponse);
}

message GetVfsDirectoryRequest {
  VfsPath path = 1;
  string cursor = 2;

  int32 offset = 3;
  int32 count = 4;
}

message GetVfsDirectoryResponse {
  repeated string subdirs = 1;
  repeated string books = 2;
  string plugin_instance_id = 3;
  string cursor = 4;
  uint64 position = 5;
  uint64 estimated_total = 6;
}

message GetVfsParentsRequest {
  SourceMetadata effective_metadata = 1;
}

message GetVfsParentsResponse {
  repeated VfsPath path = 1;
}

service VfsProviderService {
  rpc GetVfsDirectory(GetVfsDirectoryRequest) returns (GetVfsDirectoryResponse);
  rpc GetVfsParents(GetVfsParentsRequest) returns (GetVfsParentsResponse);
}

message GetServiceTranscodeTargetsRequest {
  SourceMetadata metadata = 1;
}

message GetServiceTranscodeTargetsResponse {
  repeated TranscodeTarget targets = 1;
}

message StartServiceBookTranscodeRequest {
  string src_path = 1;
  string dest_path = 2;
  FileType file_type = 3;
  repeated ConfigValue config = 4;
}

message StartServiceBookTranscodeResponse {
  string transcode_id = 1;
}

message CancelServiceBookTranscodeRequest {
  string transcode_id = 1;
}

message CancelServiceBookTranscodeResponse {

}

message GetServiceBookTranscodesRequest {

}

message GetServiceBookTranscodesResponse {
  repeated TranscodeRecord records = 1;
}

service TranscoderService {
  rpc GetServiceTranscodeTargets(GetServiceTranscodeTargetsRequest) returns (GetServiceTranscodeTargetsResponse);
  rpc StartServiceBookTranscode(StartServiceBookTranscodeRequest) returns (StartServiceBookTranscodeResponse);
  rpc CancelServiceBookTranscode(CancelServiceBookTranscodeRequest) returns (CancelServiceBookTranscodeResponse);
  rpc GetServiceBookTranscodes(GetServiceBookTranscodesRequest) returns (GetServiceBookTranscodesResponse);
}
